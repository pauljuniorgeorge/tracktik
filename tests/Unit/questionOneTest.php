<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\Television;
use App\Models\Microwave;
use App\Models\Console;
use App\Models\ElectronicItems;
use App\Models\Controller;

class questionOneTest extends TestCase
{
    
    public function testQuestionOneAndTwo(){

        //Begin Question 1
        $items = [];
        $console = new Console(1000);
        $console->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_WIRED));
        $console->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_WIRED));
        $console->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $console->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $items[] = $console;

        //Test Max Extras by attempting to add a 5th extra
        $this->assertEquals(false,$console->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE)));

        $televisionOne = new Television(600);
        $televisionOne->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $televisionOne->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $items[] = $televisionOne;

        $televisionTwo = new Television(800);
        $televisionTwo->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $items[] = $televisionTwo;

        $microwave = new Microwave(120);
        $items[] = $microwave;
        $this->assertEquals(false,$microwave->setExtra(new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE)));

        $cart = new ElectronicItems($items);
        $shoppingCart = $cart->getSortedItems();
        $this->assertEquals(2520,$cart->getTotal());
        
        //Assert Sort order is correct
        $lastItem = reset($shoppingCart);
        $count = 0 ;
        foreach($shoppingCart as $key => $item){
            if($count == 0){
                $count++;
                continue;
            }
            $this->assertTrue($item->getPrice() > $lastItem->getPrice());
            $lastItem = $item;
            $count++;
        }

        // Verify correct number of Extras
        $consoleWiredControllers = $console->getExtrasByType(Controller::ELECTRONIC_CONTROLLER_WIRED);
        $this->assertEquals(2,count($consoleWiredControllers));
        $consoleRemoteControllers = $console->getExtrasByType(Controller::ELECTRONIC_CONTROLLER_REMOTE);
        $this->assertEquals(2,count($consoleRemoteControllers));
        $televisionOneRemoteControllers = $televisionOne->getExtrasByType(Controller::ELECTRONIC_CONTROLLER_REMOTE);
        $this->assertEquals(2,count($televisionOneRemoteControllers));
        $televisionTwoRemoteControllers = $televisionTwo->getExtrasByType(Controller::ELECTRONIC_CONTROLLER_REMOTE);
        $this->assertEquals(1,count($televisionTwoRemoteControllers));

        // Output for question 1
        echo "Question 1 Total: ".$cart->getTotal()."\r\n";

        // Begin Question 2
        $microwaves = $cart->getItemsByType(Microwave::ELECTRONIC_ITEM_MICROWAVE);
        $microwave = reset($microwaves);
        //Output Question 2
        $this->assertEquals($microwave->getPrice(),120);
        echo "Question 2 Total: ".$microwave->getPrice()."\r\n";
       
    }

    public function testNoExtraLimitForTelevisions(){
        $televisionOne = new Television(600);
        for($i=0;$i<1000;$i++){
            $this->assertNotEquals(false,$televisionOne->setExtra(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        }
        $this->assertEquals(1000,count($televisionOne->getExtras()));
    }

    public function testControllerCantHaveExtras(){

        $controller = new Controller(Controller::ELECTRONIC_CONTROLLER_WIRED);
        $this->assertEquals(false,$controller->setExtra(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $this->assertEquals(false,$controller->setExtra(Controller::ELECTRONIC_CONTROLLER_WIRED));

        $controller = new Controller(Controller::ELECTRONIC_CONTROLLER_REMOTE);
        $this->assertEquals(false,$controller->setExtra(Controller::ELECTRONIC_CONTROLLER_REMOTE));
        $this->assertEquals(false,$controller->setExtra(Controller::ELECTRONIC_CONTROLLER_WIRED));

    }


}
