<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectronicItem extends Model
{
    use HasFactory;

     /** 
 * @var float 
 */ 
 public $price; 
 public $maxExtras;

  /** 
 * @var array 
 */ 
 public $extras = [];
  
 /** 
 * @var string 
 */ 
 private $type; 
 public $wired; 
  
 const ELECTRONIC_ITEM_TELEVISION = 'television'; 
 const ELECTRONIC_ITEM_CONSOLE = 'console'; 
 const ELECTRONIC_ITEM_MICROWAVE = 'microwave'; 
 const ELECTRONIC_CONTROLLER_WIRED = 'wired';
 const ELECTRONIC_CONTROLLER_REMOTE = 'remote';
  
 private static $types = array(self::ELECTRONIC_ITEM_CONSOLE,self::ELECTRONIC_ITEM_MICROWAVE, self::ELECTRONIC_ITEM_TELEVISION, self::ELECTRONIC_CONTROLLER_WIRED,self::ELECTRONIC_CONTROLLER_REMOTE);   

 public function __construct($price) {
    $this->price = $price;
 }
  
 function getPrice() 
 { 
 return $this->price; 
 } 
  
 function getType() 
 { 
 return $this->type; 
 } 

 public static function getTypes()
  {
    return self::$types;
  }
  
 function getWired() 
 { 
 return $this->wired; 
 } 
 function getMaxExtras() 
 { 
 return $this->maxExtras; 
 } 
 function getExtras(){
    return $this->extras;
}  

function getExtrasByType($type){
    
    if (in_array($type, [self::ELECTRONIC_CONTROLLER_WIRED,self::ELECTRONIC_CONTROLLER_REMOTE]))  { 
     
        $callback = function($item) use ($type)  { 
            return $item->type == $type; 
        }; 
         
        $items = array_filter($this->getExtras(), $callback);  
        return $items;
    } 
        return false; 
}  

 function setMaxExtras($maxExtras) 
 { 
 $this->maxExtras = $maxExtras; 
 } 
  
 function setPrice($price) 
 { 
 $this->price = $price; 
 } 
  
 function setType($type) 
 { 
 $this->type = $type; 
 } 
  
 function setWired($wired) 
 { 
 $this->wired = $wired; 
 } 

 function setExtra($extra){

    if($this->getMaxExtras() === 0){
        return false;
    }

    if( count($this->extras) < $this->getMaxExtras() || $this->getMaxExtras() === false){
        array_push($this->extras,$extra);
        return true;
    }
    else{
        return false;
    }

  

}

}
