<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Controller extends ElectronicItem
{
    use HasFactory;


    public function __construct($type) {
        parent::__construct(0);
        parent::settype($type);
        $this->maxExtras = 0;
    }


    }
