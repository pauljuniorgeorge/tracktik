<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElectronicItems extends Model
{
    private $items = array(); 
    public $total;
  
    public function __construct(array $items) 
    { 
        $this->items = $items; 
    } 
     
    /** 
    * Returns the items depending on the sorting type requested  * 
    * @return array 
    */ 
    public function getSortedItems() 
    { 
     
    $sorted = array(); 
    foreach ($this->items as $item) 
    { 
     $sorted[($item->price * 100)] = $item; 
    } 
     
        ksort($sorted, SORT_NUMERIC); 
        return  $sorted;
    } 
     
    /** 
    * 
    * @param string $type 
    * @return array 
    */ 
    public function getItemsByType($type) 
    { 
     
    if (in_array($type, ElectronicItem::getTypes()))  { 
     
    $callback = function($item) use ($type)  { 
     
    return $item->type == $type; 
    }; 
     
    $items = array_filter($this->items, $callback);  } 
  
    return $items; 
    } 

    public function getTotal(){
        
        $this->total = 0;
       
        foreach ($this->items as $item) 
        {  
         $this->total = $this->total + $item->price; 
        } 

        return $this->total;
    }
   
}
