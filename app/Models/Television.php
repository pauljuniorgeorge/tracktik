<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ElectronicItem;

class Television extends ElectronicItem
{
    use HasFactory;
    

    public function __construct($price) {

        parent::__construct($price);
        $this->type = self::ELECTRONIC_ITEM_TELEVISION;
        $this->maxExtras = false;
     }

}
